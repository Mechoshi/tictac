import { BOT } from "../constants";

export class Bot {
  constructor() {
    this.player = BOT;
  }

  makeMove(position) {
    const nextPositions = [...position.children];

    nextPositions.sort((a, b) => a.value - b.value);

    return nextPositions[0];
  }
}
