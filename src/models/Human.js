import { EMPTY, HUMAN } from "../constants";

export class Human {
  constructor() {
    this.player = HUMAN;
  }

  makeMove(position, index) {
    let selectedPosition = null;

    if (position.state[index] === EMPTY) {
      selectedPosition = position.children.find((child) => {
        return child.state[index] === this.player;
      });
    }

    return selectedPosition;
  }
}
