import { BOT_TIMEOUT, EMPTY, ORDER } from "../constants";
import { getInitialState, minMax } from "../helpers";
import { Bot, Human, Position } from "../models";

export class Game {
  constructor({ renderer, startPlayer }) {
    this.bot = null;
    this.currentPosition = null;
    this.human = null;
    this.initialState = getInitialState();
    this.positionsTree = null;
    this.renderer = renderer;
    this.startPlayer = startPlayer;
  }

  init() {
    this.positionsTree = new Position(this.startPlayer, this.initialState);

    minMax(this.positionsTree, ORDER ** 2);

    this.currentPosition = this.positionsTree;

    this.renderer.setGame(this);
    this.renderer.renderState();

    this.human = new Human();
    this.bot = new Bot();
  }

  play() {
    if (
      !this.currentPosition ||
      (!this.currentPosition.isWin &&
        !this.currentPosition.state.includes(EMPTY))
    ) {
      return this.renderer.renderDrawMessage();
    }

    if (this.currentPosition.isWin) {
      return this.renderer.renderGameOverMessage();
    }

    if (this.currentPosition.player === this.bot.player) {
      this.renderer.renderBotTurnMessage();

      setTimeout(() => {
        if (this.currentPosition) {
          this.currentPosition = this.bot.makeMove(this.currentPosition);

          if (this.currentPosition) {
            this.renderer.renderState();
          }

          this.play();
        }
      }, BOT_TIMEOUT);
    } else {
      this.renderer.renderHumanTurnMessage();
    }
  }
}
