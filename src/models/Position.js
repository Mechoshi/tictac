import { EMPTY, HUMAN } from "../constants";
import { getAvailableMoves, getTheOtherPlayer, isWin } from "../helpers";

export class Position {
  constructor(player, state) {
    this.player = player;
    this.state = state;
    this.value = null;

    this.options = getAvailableMoves(this.state);

    // Determine if the other player has won
    // by choosing the current position:
    this.isWin = isWin(getTheOtherPlayer(this.player), this.state);

    if (this.isWin) {
      const remainingEmptyBoxes = state.filter((box) => box === EMPTY).length;
      // Favour early victory:
      this.value = (this.player === HUMAN ? -1 : 1) * (remainingEmptyBoxes + 1);
    }

    if (this.isWin) {
      this.children = [];
    } else {
      // Construct Positions for every possible
      // move of the current player:
      this.children = this.options.map((index) => {
        const nextState = [...this.state];
        nextState[index] = this.player;

        return new Position(getTheOtherPlayer(this.player), nextState);
      });
    }
  }
}
