import { BOT, HUMAN } from "../constants";

export const minMax = (position, depth) => {
  if (depth === 0 || position.isWin) {
    return position.value || 0;
  }

  if (position.player === HUMAN) {
    let maxEval = Number.NEGATIVE_INFINITY;

    for (const child of position.children) {
      const eval = minMax(child, depth - 1);
      child.value = eval;
      maxEval = Math.max(maxEval, eval);
    }

    return maxEval;
  }

  if (position.player === BOT) {
    let minEval = Number.POSITIVE_INFINITY;

    for (const child of position.children) {
      const eval = minMax(child, depth - 1);
      child.value = eval;
      minEval = Math.min(minEval, eval);
    }

    return minEval;
  }
};
