export * from "./getAvailableMoves";
export * from "./getInitialState";
export * from "./getTheOtherPlayer";
export * from "./isWin";
export * from "./minMax";
