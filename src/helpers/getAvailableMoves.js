import { EMPTY } from "../constants";

export const getAvailableMoves = (state) =>
  state.reduce((availableMoves, current, index) => {
    if (current === EMPTY) {
      availableMoves.push(index);
    }

    return availableMoves;
  }, []);
