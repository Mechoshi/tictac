import { BOT, HUMAN } from "../constants";

export const getTheOtherPlayer = (previousPlayer) =>
  previousPlayer === HUMAN ? BOT : HUMAN;
