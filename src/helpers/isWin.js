import { ORDER } from "../constants";

export const isWin = (player, state) => {
  // check rows:
  for (let i = 0; i < state.length; i += ORDER) {
    let win = true;

    for (let j = 0; j < ORDER; j++) {
      win = win && state[i + j] === player;
    }

    if (win) {
      return true;
    }
  }

  // check columns:
  for (let i = 0; i < ORDER; i++) {
    let win = true;

    for (let j = 0; j < state.length; j += ORDER) {
      win = win && state[i + j] === player;
    }

    if (win) {
      return true;
    }
  }

  // check main diagonal:
  let winMainDiag = true;
  for (let i = 0; i < ORDER; i++) {
    winMainDiag = winMainDiag && state[ORDER * i + i] === player;
  }

  if (winMainDiag) {
    return true;
  }

  // check secondary diagonal:
  let winSecondaryDiag = true;
  for (let i = 0; i < ORDER; i++) {
    winSecondaryDiag =
      winSecondaryDiag && state[(ORDER - 1) * (i + 1)] === player;
  }

  if (winSecondaryDiag) {
    return true;
  }

  return false;
};
