import { EMPTY, ORDER } from "../constants";

export const getInitialState = () => new Array(ORDER ** 2).fill(EMPTY);
