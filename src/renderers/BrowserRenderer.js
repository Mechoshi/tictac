import { BOT, HUMAN } from "../constants";
import { getTheOtherPlayer } from "../helpers";

export class BrowserRenderer {
  constructor() {
    this.board = document.getElementById("board");
    this.game = null;
    this.messageContainer = document.getElementById("message");

    this.board.addEventListener("click", (event) => {
      if (event.target.classList.contains("empty")) {
        this.game.currentPosition = this.game.human.makeMove(
          this.game.currentPosition,
          event.target.dataset.index
        );

        this.renderState();

        this.game.play();
      }
    });

    const replayButton = document.getElementById("replay");

    replayButton.addEventListener("click", () => {
      this.game.init();
      this.game.play();
    });
  }

  getClass(box) {
    switch (box) {
      case HUMAN:
        return "maximizer";
      case BOT:
        return "minimizer";
      default:
        return "empty";
    }
  }

  renderBotTurnMessage() {
    this.messageContainer.innerHTML = `
          <p class="bot-turn">Bot turn</p>
          `;
    this.board.classList.add("bot-turn");
  }

  renderDrawMessage() {
    this.messageContainer.innerHTML = `
          <p class="draw">Draw!</p>
          `;
  }

  renderHumanTurnMessage() {
    this.board.classList.remove("bot-turn");
    this.messageContainer.innerHTML = `
          <p class="your-turn">Human turn</p>
          `;
  }

  renderGameOverMessage() {
    this.messageContainer.innerHTML = `
          <p class="game-over">Game over! ${
            getTheOtherPlayer(this.game.currentPosition.player) ===
            this.game.bot.player
              ? "Bot"
              : "Human"
          } wins!</p>
          `;
  }

  renderState() {
    this.board.innerHTML = "";

    this.board.innerHTML = `
  ${this.game.currentPosition.state
    .map((box, index) => {
      return `<div data-index="${index}" class="${this.getClass(box)}"></div>`;
    })
    .join("")}
  `;
  }

  setGame(game) {
    this.game = game;
  }
}
