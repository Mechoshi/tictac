import { Game } from "./models";
import { BrowserRenderer } from "./renderers";

import { HUMAN } from "./constants";

import "./styles.scss";

window.onload = () => {
  const game = new Game({
    renderer: new BrowserRenderer(),
    startPlayer: HUMAN,
  });

  game.init();
  game.play();
};
